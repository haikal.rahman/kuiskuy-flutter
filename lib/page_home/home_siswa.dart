import 'package:flutter/material.dart';
import 'package:kuiskuy_flutter/bareng_bareng/drawer_kuiskuy.dart';
import 'package:kuiskuy_flutter/bareng_bareng/appbar_kuiskuy.dart';


class home_siswa extends StatefulWidget {
  const home_siswa({Key? key}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  @override
  State<home_siswa> createState() => _home_siswa();
}

class _home_siswa extends State<home_siswa> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController kodeController = TextEditingController();
  String kode = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const DrawerKuiskuy(),
      appBar: const AppbarKuiskuy("Home Siswa"),
      body: Container(
          height: 1000,
          width: double.infinity,
          decoration: const BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Colors.lightBlue, Colors.purple]),
                ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              // flying box
              Container(
                width: 300,
                height: 300,
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(211, 215, 248, 0.6),
                  borderRadius: BorderRadius.circular(18),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.06),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: const Offset(0, 3), // changes position of shadow
                    ),
                  ],
                ),
                // isi dari flying box
                child: ListView(
                  children: <Widget>[
                    Container(
                      padding: const EdgeInsets.only(top: 60),
                      alignment: Alignment.center,
                      child: const Text("Hi, Student!",
                          style: TextStyle(
                              fontFamily: 'Satisfy-Regular',
                              fontSize: 45,
                              color: Colors.purple)),
                    ),


                    Container(
                      padding: const EdgeInsets.only(left: 40, right: 40, top: 15),
                      child: TextField(
                        controller: kodeController,
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            hintText: 'Masukkan kode kuis disini',
                            hintStyle: TextStyle(color: Colors.deepPurple),
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 15)),
                      ),
                    ),


                    Container(
                      height: 60,
                      padding: const EdgeInsets.only(top: 20, left: 70, right: 70),
                      child: ElevatedButton(
                        child: const Text('Enter'),
                        onPressed: () {
                          print(kodeController.text);
                        },
                      ),
                    )
                  ],
                ),
              )
            ],
          )

         
          ),
    );
  }
}

