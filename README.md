# Kuiskuy

Aplikasi yang kami ajukan adalah KuisKuy. KuisKuy adalah aplikasi E-Learning mirip seperti Google Class yang memudahkan proses kuis yang dilakukan antara murid dan tutor. Tujuan utama kami adalah mempermudah kegiatan belajar mengajar para pelajar khususnya di masa pandemi saat ini. Kami menyediakan website ini dengan dua role, yaitu role Pengajar yang bisa membuat, mengedit, dan mereview kuis. Pengajar juga bisa membuat mata pelajaran baru. Selain pengajar, role lain adalah pelajar. Pelajar dapat mengikuti kuis dan melihat nilai serta pembahasan kuis tersebut setelah kuis itu selesai. Selain itu, pelajar juga dapat melihat profil mereka untuk melihat statistik nilai kuis pada masing-masing pelajaran yang mereka ikuti.

## Anggota Kelompok
| Nama                              | NPM           |
| --------------------------------- |:-------------:|
| Drasseta Aliyyu Darmansyah        | 1706074953    |
| Davina Irene Butar Butar          | 2006597525    |
| Dzulandra Perkasa                 | 2006597752    |
| Gerardus Abhirama Wicaksono       | 2006597563    |
| Haikal Rahman                     | 2006597891    |
| Hanif Omar Kertapati              | 2006597481    |
| Shehaanmakya Muhammad Sutoyo R    | 2006597235    |

## Link Download
File download dapat diakses pada direktori public_release, atau dengan link<br>
https://gitlab.com/haikal.rahman/kuiskuy-flutter/-/tree/main/public_release <br>
> Sesuaikan File APK yang didownload dengan Arsitektur HP Anda

## Modul Aplikasi

1. Profile user yang berisi nilai
    1. Foto pelajar
    1. Nama lengkap pelajar
    1. Roles
    1. No induk pelajar
    1. Nilai-nilai dari kuis yang sudah dilakukan oleh pelajar(bisa berupa statistik) (hanya untuk pelajar)
1. Page Home (pelajar)
    1. Kumpulan mata pelajaran
1. Page Home (pembuat soal)
    1. Kumpulan matkul
1. Page Kuis
    1. Soal
    1. Jawaban
1. Page Pembahasan
    1. Soal
    1. Jawaban
    1. Pembahasan
1. Landing Page
    1. Login Form, sama tombol register redirect ke page register akun
    1. About us
    1. Contact us
1. Register Akun (Form)
    1. Text box pengisian email
    1. Text box pengisian password
    1. Text box konfirmasi password
1. Register Soal (Page pembuatan soal)
    1. Soal 
    1. Jawaban<br>
    Poin setiap jawaban, tombol show/hide poin jawaban (poin tambah, atau poin kurang jika salah) 
    1. Pembahasan

## Pembagian Tugas Modul
| Modul                     | Nama          |
| ------------------------- |:-------------:|
| Page Kuis                 | Anky          |
| Page Home Pelajar         | Davina        |
| Page Home Pembuat Soal    | Davina        |
| Landing Page              | Dzul          |
| Page Pembahasan           | Gerard        |
| Profile Page Pelajar      | Haikal        |
| Profile Page Pembuat Soal | Haikal        |
| Page Register Soal        | Hanif         |
| Page Register Akun        | Bersama-sama  |

## Integrasi dengan Web Service
Pengiriman data dari web dilakukan dengan menggunakan Django JSON Serliazier ketika data dibutuhkan. Menggunakan konsep async untuk performa dan UX yang lebih baik, data tersebut diterima dan diextract oleh flutter menjadi objek Map. Flutter tersebut sebelumnya import http package, dan ditambahkan perizinan penggunaan internet pada AndroidManifest.xml.

## GOALS 
Memberikan platform yang efektif untuk para pelajar yang sedang mengikuti kegiatan belajar mengajar secara daring.

## MOTIVATIONS
Pelajar menginginkan aplikasi belajar yang mudah digunakan.
Pelajar menginginkan aplikasi belajar yang memberikan pembahasan.
Kondisi pandemi yang mendorong pelajar lebih sering belajar dari rumah.