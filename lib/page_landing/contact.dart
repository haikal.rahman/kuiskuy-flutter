import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:kuiskuy_flutter/bareng_bareng/drawer_kuiskuy.dart';
import 'package:kuiskuy_flutter/bareng_bareng/appbar_kuiskuy.dart';
import 'package:http/http.dart' as http;


class Contact extends StatelessWidget {
  const Contact({Key? key}) : super(key: key);
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Contact Form',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Contact Form'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Future sendEmail({
    required String firstName,
    required String lastName,
    required String email,
    required String message,
  }) async{
    final serviceId = "service_gxmrkhi";
    final templateId = "template_qldmtgh";
    const userId = "user_HHbPP8ueFAowKimK2qswe";

    final url = Uri.parse("https://api.emailjs.com/api/v1.0/email/send");
    final response = await http.post(
      url,
      headers: {
        "origin": "http://localhost",
        "Content-Type" : "application/json"
      },
      body: json.encode({
        "user_id": userId,
        "service_id" : serviceId,
        "template_id": templateId,
        "template_params":{
          "user_firstName" : firstName,
          "user_lastName" : lastName,
          "user_email" : email,
          "user_message": message
        }
      })
      );
      print(response.body);
  }
  @override
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormState>();
    final firstNameController = TextEditingController();
    final lastNameController = TextEditingController();
    final emailController = TextEditingController();
    final messageController = TextEditingController();

    return Scaffold(
      drawer: const DrawerKuiskuy(),
      appBar: AppBar(
       
          leading:
          IconButton( onPressed: (){
            Navigator.pop(context);
          },icon:Icon(Icons.arrow_back_ios,size: 20,color: Colors.black,)),
        ),
      body: Form(
        key: _formKey,
        child: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Colors.lightBlue, Colors.purple]),
                ),
          child: Padding(
            padding: const EdgeInsets.all(50.0),
            child: SingleChildScrollView(
              reverse: true,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                const Text("Contact Us", style: TextStyle(
                fontWeight: FontWeight.bold,
                  fontSize: 32.0
                  ),
                ),
                const Text("Contact us directly if you have any questions"),
                const SizedBox(height: 20.0),
                Column(
                  children: <Widget>[
                    TextFormField(
                      controller: firstNameController,
                      decoration:InputDecoration(
                        contentPadding: const EdgeInsets.fromLTRB(20.0, 15.0, 15.0, 15.0),
                        labelText: "First Name",
                        hintText: "type your first name here",
                        filled: true,
                        border:
                        OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
                        validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please enter your first name';
                      }
                      return null;
                    },
                    ),
                    const SizedBox(height: 16.0),
                    TextFormField(
                      controller: lastNameController,
                      decoration:InputDecoration(
                        contentPadding: const EdgeInsets.fromLTRB(20.0, 15.0, 15.0, 15.0),
                        labelText: "Last Name",
                        hintText: "type your last name here",
                        filled: true,
                        border:
                        OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
                        validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please enter your last name';
                      }
                      return null;
                    },
                    ),
                    const SizedBox(height: 16.0),
                    TextFormField(
                      controller: emailController,
                      decoration:InputDecoration(
                        contentPadding: const EdgeInsets.fromLTRB(20.0, 15.0, 15.0, 15.0),
                        labelText: "E-mail",
                        hintText: "Enter your active email address here",
                        filled: true,
                        border:
                        OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
                        validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please Input your e-mail so we can contact you';
                      }
                      return null;
                    },
                    ),
                    const SizedBox(height: 16.0),
                    TextFormField(
                      controller: messageController,
                      maxLength: 3000,
                      maxLines: 14,
                      decoration:InputDecoration(
                        contentPadding: const EdgeInsets.fromLTRB(20.0, 15.0, 15.0, 15.0),
                        labelText: "Message",
                        hintText: "Please input your message here",
                        filled: true,
                        border:
                        OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
                        validator: (value) {
                      if (value!.isEmpty) {
                        return 'Message can not be empty!';
                      }
                      return null;
                    },
                    ),
                    const SizedBox(height: 10.0),
                    MaterialButton(
                      height: 30,
                      color: Colors.black,
                      minWidth: double.infinity,
                      onPressed:() { sendEmail(
                        firstName: firstNameController.text, 
                        lastName: lastNameController.text, 
                        email: emailController.text, 
                        message: messageController.text
                      );
                      },
                      child: const Text(
                        "SUBMIT",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                      ),
                  ],
                ),
              ],
            )
           ),
          ),
        )       // This trailing comma makes auto-formatting nicer for build methods.
      )
    );
  }
}
