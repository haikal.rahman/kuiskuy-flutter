import 'package:flutter/material.dart';

class AppbarKuiskuy extends StatelessWidget implements PreferredSizeWidget {
  const AppbarKuiskuy(this.title, {Key? key}) : super(key: key);
  final String title;


  @override
  Size get preferredSize{
    return Size.fromHeight(AppBar().preferredSize.height);
  } 

  @override
  Widget build(BuildContext context) {
    return AppBar(
      // Here we take the value from the MyHomePage object that was created by
      // the App.build method, and use it to set our appbar title.
      title: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Text(
          title,
          style: const TextStyle(color: Colors.black),
        ),
        Image.asset(
          'assets/images/transparent_kuiskuy_logo.png',
          height: 50,
          width: 50,
        ),
      ]),
      backgroundColor: Colors.white,
      iconTheme: const IconThemeData(color: Colors.black),
    );
  }
}
