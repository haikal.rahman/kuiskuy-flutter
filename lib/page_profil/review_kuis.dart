import 'package:flutter/material.dart';
import 'package:kuiskuy_flutter/bareng_bareng/drawer_kuiskuy.dart';
import 'package:kuiskuy_flutter/bareng_bareng/appbar_kuiskuy.dart';
import 'package:kuiskuy_flutter/bareng_bareng/user_data.dart';
import 'package:kuiskuy_flutter/page_pembahasan/PembahasanScreen.dart';

class ReviewPage extends StatefulWidget {
  const ReviewPage({Key? key}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  @override
  State<ReviewPage> createState() => _ReviewPageState();
}

class _ReviewPageState extends State<ReviewPage> with TickerProviderStateMixin {
  Map<String, Map<String, Map<String, String>>> dataFetch =
      UserData().matakuliah;
  List<List<dynamic>> data = [];
  String _title = "Pilih Matakuliah";
  String _tooltip = "Klik matakuliah untuk melihat daftar kuis";
  List<Widget> _buttons = [];

  @override
  void initState() {
    _buttons = _matkulButtons();
    super.initState();
  }

  void _munculkanTombolMatkul() {
    setState(() {
      _title = "Pilih Matakuliah";
      _tooltip = "Klik matakuliah untuk melihat daftar kuis";
      _buttons = _matkulButtons();
    });
  }

  void _munculkanTombolKuis(int idx) {
    setState(() {
      _title = data[idx][0];
      _tooltip = "Klik kuis untuk melihat pembahasan";
      _buttons = _kuisButtons(idx);
    });
  }

  List<Widget> _matkulButtons() {
    data = _listify();
    List<Widget> ret = [];
    List<String> months = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    ];
    for (int i = 0; i < data.length; i++) {
      String lastQuizName = data[i][1][data[i][1].length - 1][0];
      String lastQuizDate = data[i][1][data[i][1].length - 1][2];
      lastQuizDate = months[int.parse(lastQuizDate.substring(5, 7)) - 1] +
          " " +
          lastQuizDate.substring(8) +
          ", " +
          lastQuizDate.substring(0, 4);

      ElevatedButton tmp = ElevatedButton(
          child: SizedBox(
              width: 200,
              child: Padding(
                  padding: const EdgeInsets.all(5),
                  child: Column(children: [
                    Text(data[i][0],
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontSize: 20)),
                    Column(children: [
                      const Text("Banyak kuis dikerjakan: ",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 10, color: Colors.blue)),
                      Text(data[i][1].length.toString(),
                          textAlign: TextAlign.center,
                          style: const TextStyle(fontSize: 15))
                    ]),
                    Column(children: [
                      const Text("Kuis terakhir: ",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 10, color: Colors.blue)),
                      Text(lastQuizName,
                          textAlign: TextAlign.center,
                          style: const TextStyle(fontSize: 15)),
                      Text(lastQuizDate,
                          textAlign: TextAlign.center,
                          style: const TextStyle(
                              fontSize: 10, color: Colors.blue)),
                    ]),
                  ]))),
          onPressed: () => _munculkanTombolKuis(i));
      ret.add(tmp);
      SizedBox gap = const SizedBox(height: 10);
      ret.add(gap);
      if (i == data.length - 1) {
        FloatingActionButton backbutton = FloatingActionButton(
            heroTag: 'ScrollBtn',
            onPressed: () => Navigator.pop(context),
            child: const Icon(Icons.arrow_back_rounded),
            backgroundColor: Colors.blue,
            tooltip: "Kembali pilih matakuliah");
        ret.add(backbutton);
      }
    }
    return ret;
  }

  List<Widget> _kuisButtons(int idxMatkul) {
    List<Widget> ret = [];
    List<String> months = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    ];
    for (int i = 0; i < data[idxMatkul][1].length; i++) {
      String quizName = data[idxMatkul][1][i][0];
      String quizScore = data[idxMatkul][1][i][1];
      String quizDate = data[idxMatkul][1][i][2];
      quizDate = months[int.parse(quizDate.substring(5, 7)) - 1] +
          " " +
          quizDate.substring(8) +
          ", " +
          quizDate.substring(0, 4);

      ElevatedButton tmp = ElevatedButton(
          child: SizedBox(
              width: 200,
              child: Padding(
                  padding: const EdgeInsets.all(5),
                  child: Column(children: [
                    Text(quizName,
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontSize: 20)),
                    Column(children: [
                      const Text("Nilai kuis: ",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 10, color: Colors.blue)),
                      Text(quizScore + "/100",
                          textAlign: TextAlign.center,
                          style: const TextStyle(fontSize: 15))
                    ]),
                    Column(children: [
                      const Text("Dikerjakan pada: ",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 10, color: Colors.blue)),
                      Text(quizDate,
                          textAlign: TextAlign.center,
                          style: const TextStyle(fontSize: 15)),
                    ]),
                  ]))),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => PembahasanScreen()),
            );
          }
          );
      ret.add(tmp);
      SizedBox gap = const SizedBox(height: 10);
      ret.add(gap);
      if (i == data[idxMatkul][1].length - 1) {
        FloatingActionButton backbutton = FloatingActionButton(
            heroTag: 'ScrollBtn',
            onPressed: () => _munculkanTombolMatkul(),
            child: const Icon(Icons.arrow_back_rounded),
            backgroundColor: Colors.blue,
            tooltip: "Kembali pilih matakuliah");
        ret.add(backbutton);
      }
    }
    return ret;
  }

  //[[matkul1, [[kuis1, nilai, tgl], [kuis2, nilai, tgl]]],...]
  List<List<dynamic>> _listify() {
    List<List<dynamic>> hasil = [];
    Iterable<String> matkulKeys = dataFetch.keys;
    for (int i = 0; i < matkulKeys.length; i++) {
      List<dynamic> mat = [];
      mat.add(matkulKeys.elementAt(i));
      List<List<String>> ku = [];
      Iterable<String> kuisKeys = dataFetch[matkulKeys.elementAt(i)]!.keys;
      for (int j = 0; j < kuisKeys.length; j++) {
        List<String> detail = [];
        detail.add(kuisKeys.elementAt(j));
        detail.add(dataFetch[matkulKeys.elementAt(i)]![kuisKeys.elementAt(j)]![
            "nilai"]!);
        detail.add(dataFetch[matkulKeys.elementAt(i)]![kuisKeys.elementAt(j)]![
            "tgl"]!);
        ku.add(detail);
      }
      mat.add(ku);
      hasil.add(mat);
    }
    return hasil;
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      drawer: const DrawerKuiskuy(),
      appBar: const AppbarKuiskuy("Review Kuis"),
      body: SingleChildScrollView(
        physics: const NeverScrollableScrollPhysics(),
        child: Container(
          // Center is a layout widget. It takes a single child and positions it
          // in the middle of the parent.
          child: Column(
            // Column is also a layout widget. It takes a list of children and
            // arranges them vertically. By default, it sizes itself to fit its
            // children horizontally, and tries to be as tall as its parent.
            //
            // Invoke "debug painting" (press "p" in the console, choose the
            // "Toggle Debug Paint" action from the Flutter Inspector in Android
            // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
            // to see the wireframe for each widget.
            //
            // Column has various properties to control how it sizes itself and
            // how it positions its children. Here we use mainAxisAlignment to
            // center the children vertically; the main axis here is the vertical
            // axis because Columns are vertical (the cross axis would be
            // horizontal).
            children: <Widget>[
              Container(
                  child: Column(
                    children: [
                      Text(
                        _title,
                        style: const TextStyle(
                          fontSize: 20,
                          color: Colors.black,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      Text(
                        _tooltip,
                        style: const TextStyle(
                          fontSize: 10,
                          color: Colors.black,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      const SizedBox(height: 15),
                      SizedBox(
                          height: 430,
                          child: SingleChildScrollView(
                            child: Column(
                              children: _buttons,
                            ),
                            scrollDirection: Axis.vertical,
                          ))
                    ],
                  ),
                  padding:
                      const EdgeInsets.symmetric(vertical: 30, horizontal: 20),
                  decoration: BoxDecoration(
                      color: const Color.fromRGBO(211, 215, 248, 0.6),
                      borderRadius: BorderRadius.circular(18),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.06),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: const Offset(0, 3),
                        )
                      ]),
                  width: double.infinity,
                  height: 590),
              const SizedBox(height: 70)
            ],
          ),
          decoration: const BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.lightBlue, Colors.purple]),
          ),
          alignment: Alignment.center,
          padding: const EdgeInsets.symmetric(vertical: 40, horizontal: 20),
        ),
      ),
    );
  }
}
