import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:kuiskuy_flutter/page_kuis/constant.dart';
import 'package:kuiskuy_flutter/page_kuis/screens/quiz/quiz_screen.dart';
import 'package:kuiskuy_flutter/bareng_bareng/drawer_kuiskuy.dart';
import 'package:kuiskuy_flutter/bareng_bareng/appbar_kuiskuy.dart';
import 'package:kuiskuy_flutter/bareng_bareng/user_data.dart';

class StartScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:Colors.purple,
      drawer: const DrawerKuiskuy(),
      appBar: const AppbarKuiskuy("Kuis"),
      body: Container(
        child:   Stack(
          children: [
            SafeArea(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: kDefaultPadding),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Spacer(flex: 2), //2/6
                    Text(
                      "KuisKuy",
                      style: Theme.of(context).textTheme.headline4!.copyWith(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                    Spacer(), // 1/6
                    InkWell(
                      onTap: () => Get.to(QuizScreen()),
                      child: Container(
                        width: double.infinity,
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(kDefaultPadding * 0.75), // 15
                        decoration: BoxDecoration(
                          gradient: kPrimaryGradient,
                          borderRadius: BorderRadius.all(Radius.circular(12)),
                        ),
                        child: Text(
                          "Mari Memulai Kuis",
                          style: Theme.of(context)
                              .textTheme
                              .button
                              !.copyWith(color: Colors.black),
                        ),
                      ),
                    ),
                    Spacer(flex: 2), // it will take 2/6 spaces
                  ],
                ),
              ),
            ),
          ],
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(18),
          gradient: const LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [Colors.lightBlue, Colors.purple])),
      ) 
    );
  }
}
