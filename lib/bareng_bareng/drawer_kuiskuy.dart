import 'package:flutter/material.dart';
import 'package:kuiskuy_flutter/bareng_bareng/user_data.dart';
import 'package:kuiskuy_flutter/page_home/home_siswa.dart';
import 'package:kuiskuy_flutter/page_pembahasan/PembahasanScreen.dart';
import 'package:kuiskuy_flutter/page_profil/profile_siswa.dart';
import 'package:kuiskuy_flutter/page_profil/edit_profile_page.dart';
import 'package:kuiskuy_flutter/page_kuis/screens/start/start_screen.dart';
import 'package:kuiskuy_flutter/page_landing/contact.dart';
import 'package:kuiskuy_flutter/page_landing/about.dart';
import 'package:kuiskuy_flutter/page_daftar_soal/page_daftar_soal.dart';


class DrawerKuiskuy extends StatelessWidget {
  const DrawerKuiskuy({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Material(
          child: ListView(padding: EdgeInsets.zero, children: [
        UserAccountsDrawerHeader(
          accountName: Text(UserData().nama),
          accountEmail: Text(UserData().email),
          currentAccountPicture: CircleAvatar(
              child: ClipOval(
                  child: Image.asset(
                      'assets/images/gray_transparent_default_profile.png'))),
        ),
        ListTile(
          title: const Text("Home"),
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const home_siswa()));
      
          },
        ),
/*        ListTile(
          title: const Text("Daftar Soal"),
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const DaftarSoal()));
          },
        ),
*/        ListTile(
          title: const Text("Kuis"),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => StartScreen()));
       
          },
        ),
        ListTile(
          title: const Text("Profil"),
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const ProfileSiswa()));
          },
        ),
        ListTile(
          title: const Text("Edit Profil"),
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const EditProfilePage()));
          },
        ),
        ListTile(
          title: const Text("Pembahasan"),
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>  PembahasanScreen()));
          },
        ),
        ListTile(
          title: const Text("Contact us"),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const Contact())

            );
          },
        ),
        ListTile(
          title: const Text("About us"),
          onTap: (){
            Navigator.push(context,
              MaterialPageRoute(
                builder: (context) => const About())
            );
          },
        )
      ])),
    );
  }
}
