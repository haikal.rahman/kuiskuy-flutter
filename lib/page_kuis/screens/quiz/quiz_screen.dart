import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kuiskuy_flutter/page_kuis/controllers/question_controller.dart';
import 'package:kuiskuy_flutter/bareng_bareng/drawer_kuiskuy.dart';
import 'package:kuiskuy_flutter/bareng_bareng/appbar_kuiskuy.dart';
import 'package:kuiskuy_flutter/bareng_bareng/user_data.dart';

import 'components/body.dart';

class QuizScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    QuestionController _controller = Get.put(QuestionController());
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        // Fluttter show the back button automatically
        backgroundColor: Colors.transparent,
        elevation: 0,
        actions: [
          FlatButton(onPressed: _controller.nextQuestion, child: Text("Skip")),
        ],
      ),
      body: Body(),
    );
  }
}
