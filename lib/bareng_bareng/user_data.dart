class UserData {
  String nama = 'Bambang Uzumaki';
  String noInduk = '1234567891';
  String status = 'Siswa';
  String email = "uzumakiB4mbang@hotmail.com";
  String pass = "sayaGanteng123";
  Map<String, Map<String, Map<String, String>>> matakuliah = {
    "Operating System": {
      "Week00": {"nilai": "80", "tgl": "2021-09-06"},
      "Week01": {"nilai": "0", "tgl": "2021-09-13"},
      "Week02": {"nilai": "56.3", "tgl": "2021-09-20"},
      "Week03": {"nilai": "50", "tgl": "2021-09-27"},
      "Week04": {"nilai": "83.3", "tgl": "2021-10-04"},
      "Week05": {"nilai": "0", "tgl": "2021-10-11"},
      "Week06": {"nilai": "62.5", "tgl": "2021-11-01"},
      "Week07": {"nilai": "87.5", "tgl": "2021-11-08"},
      "Week08": {"nilai": "100", "tgl": "2021-11-15"},
      "Week09": {"nilai": "100", "tgl": "2021-11-22"},
      "Week10": {"nilai": "100", "tgl": "2021-11-29"},
    },
    "Struktur Data dan Algoritma": {
      "Lab0": {"nilai": "100", "tgl": "2021-09-03"},
      "Lab1": {"nilai": "100", "tgl": "2021-09-07"},
      "Lab2": {"nilai": "100", "tgl": "2021-09-14"},
      "Lab3": {"nilai": "7", "tgl": "2021-09-21"},
      "TP1": {"nilai": "22", "tgl": "2021-10-02"},
      "Lab4": {"nilai": "41", "tgl": "2021-10-05"},
      "Lab5": {"nilai": "0", "tgl": "2021-11-10"},
      "TP2": {"nilai": "10", "tgl": "2021-11-14"},
      "Lab6": {"nilai": "0", "tgl": "2021-11-16"},
      "Lab7": {"nilai": "100", "tgl": "2021-11-23"},
      "TP3": {"nilai": "37", "tgl": "2021-12-04"},
    },
    "Metodologi Penelitian dan Penulisan Ilmiah": {
      "TI1": {"nilai": "94", "tgl": "2021-09-28"},
      "Quiz 1": {"nilai": "80", "tgl": "2021-10-11"},
      "TI2": {"nilai": "91", "tgl": "2021-10-12"},
      "TI3": {"nilai": "87", "tgl": "2021-11-02"},
      "Quiz 2": {"nilai": "84", "tgl": "2021-11-22"},
    },
  };

  void setNama(String namaBaru) {
    nama = namaBaru;
  }

  void setNoInduk(String noIndukBaru) {
    noInduk = noIndukBaru;
  }

  void setStatus(String statusBaru) {
    status = statusBaru;
  }

  void setEmail(String emailBaru) {
    email = emailBaru;
  }

  void setPassword(String passwordBaru) {
    pass = passwordBaru;
  }
}
