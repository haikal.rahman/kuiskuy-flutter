import 'package:flutter/material.dart';
import 'package:kuiskuy_flutter/bareng_bareng/drawer_kuiskuy.dart';
import 'package:kuiskuy_flutter/bareng_bareng/appbar_kuiskuy.dart';


class About extends StatelessWidget {
  const About({Key? key}) : super(key: key);
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'About Us',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'About Us'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const DrawerKuiskuy(),
      appBar: AppBar(
       
          leading:
          IconButton( onPressed: (){
            Navigator.pop(context);
          },icon:Icon(Icons.arrow_back_ios,size: 20,color: Colors.black,)),
        ),
      body: Center(
        child:Container(
          decoration: const BoxDecoration(gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Colors.lightBlue, Colors.purple]),
                ),
          child: ListView(
            children: <Widget>[
              Container(
                padding: const EdgeInsets.only(top:110),
                alignment: Alignment.center,
                child: const Text(
                  "KuisKuy",
                  style: TextStyle(
                    color: Colors.blue,
                    fontFamily: "Satisfy-Regular",
                    fontSize: 45,                  
                  ),
                )
              ),
              Container(
                padding: const EdgeInsets.only(top:20, left: 10, right: 10),
                alignment: Alignment.center,
                child: const Text(
                  "KuisKuy merupakan platform kuis berbasis web dan mobile yang dikembangkan untuk memudahkan pelaksanaan kuis di semua jenjang",
                style: TextStyle(
                  color: Colors.black,
                  fontFamily: "Satisfy-Regular",
                  fontSize: 25
                ),
                )
              )
            ],
          ),
        ),
      )
    );
  }}