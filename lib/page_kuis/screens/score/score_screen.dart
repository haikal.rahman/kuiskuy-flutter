// // import 'package:flutter/material.dart';
// // import 'package:get/get.dart';
// // import 'package:kuiskuy_flutter/page_kuis/constant.dart';
// // import 'package:kuiskuy_flutter/page_kuis/controllers/question_controller.dart';
// // import 'package:flutter_svg/flutter_svg.dart';
// // import 'package:kuiskuy_flutter/bareng_bareng/appbar_kuiskuy.dart';
// // import 'package:kuiskuy_flutter/bareng_bareng/drawer_kuiskuy.dart';

// // class ScoreScreen extends StatelessWidget {
// //   @override
// //   Widget build(BuildContext context) {
// //     QuestionController _qnController = Get.put(QuestionController());
// //     return Scaffold(
// //       backgroundColor: Colors.purple,
// //       drawer: const DrawerKuiskuy(),
// //       appBar: const AppbarKuiskuy("Kuis"),
// //       body: Stack(
// //         fit: StackFit.expand,
// //         children: [
// //           SvgPicture.asset("assets/icons/bg.svg", fit: BoxFit.fill),
// //           Column(
// //             children: [
// //               Spacer(flex: 3),
// //               Text(
// //                 "Score",
// //                 style: Theme.of(context)
// //                     .textTheme
// //                     .headline3
// //                     !.copyWith(color: kSecondaryColor),
// //               ),
// //               Spacer(),
// //               Text(
// //                 "${_qnController.correctAns! * 10}/${_qnController.questions.length * 10}",
// //                 style: Theme.of(context)
// //                     .textTheme
// //                     .headline4
// //                     !.copyWith(color: kSecondaryColor),
// //               ),
// //               Spacer(flex: 3),
// //             ],
// //           )
// //         ],
// //       ),
// //     );
// //   }
// // }
// class ScoreScreen extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     QuestionController _qnController = Get.put(QuestionController());
//     return Scaffold(
//       backgroundColor: Colors.purple,
//       drawer: const DrawerKuiskuy(),
//       appBar: const AppbarKuiskuy("Kuis"),
//       body: Container(
//         child:  Stack(
//           fit: StackFit.expand,
//           children: [
//             Column(
//               children: [
//                 Spacer(flex: 3),
//                 Text(
//                   "Score",
//                   style: Theme.of(context)
//                       .textTheme
//                       .headline3
//                       !.copyWith(color: kSecondaryColor),
//                 ),
//                 Spacer(),
//                 Text(
//                   "${_qnController.correctAns! * 10}/${_qnController.questions.length * 10}",
//                   style: Theme.of(context)
//                       .textTheme
//                       .headline4
//                       !.copyWith(color: kSecondaryColor),
//                 ),
//                 Spacer(flex: 3),
//               ],
//             )
//           ],
//         ),
//         decoration: BoxDecoration(
//           borderRadius: BorderRadius.circular(18),
//           gradient: const LinearGradient(
//               begin: Alignment.topCenter,
//               end: Alignment.bottomCenter,
//               colors: [Colors.lightBlue, Colors.purple])),
//       ),
//     );
//   }
// }
