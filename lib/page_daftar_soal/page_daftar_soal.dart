import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart'; 

class DaftarSoal extends StatefulWidget {
  const DaftarSoal({Key? key}) : super(key: key);

  @override
  _DaftarSoalState createState() => _DaftarSoalState();
}

class _DaftarSoalState extends State<DaftarSoal> {
  final formKey = GlobalKey<FormState>();
  String questionField = '';
  String op1Field = '';
  String op2Field = '';
  String op3Field = '';
  String op4Field = '';
  String ansField = '';
  String pembahasanField = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // drawer: NavBar(),
      appBar: AppBar(
        title: const Text('Pendaftaran Soal'),
        backgroundColor: Colors.deepPurple,
      ),
      body: Container(
          height: 1000,
          width: double.infinity,
          decoration: const BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Colors.purple, Colors.lightBlue])),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              // flying box
              Container(
                  width: 300,
                  height: 450,
                  decoration: BoxDecoration(
                    color: const Color.fromRGBO(211, 215, 248, 0.6),
                    borderRadius: BorderRadius.circular(18),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.06),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset:
                            const Offset(0, 3), // changes position of shadow
                      ),
                    ],
                  ),
                  child: Form(
                    key: formKey,
                    // isi dari flying box
                    child: SingleChildScrollView(
                        child: Column(
                      children: <Widget>[
                        Container(
                          padding: const EdgeInsets.only(top: 60),
                          alignment: Alignment.center,
                          child: const Text("Pertanyaan",
                              style: TextStyle(
                                  fontFamily: 'Satisfy-Regular',
                                  // fontSize: 45,
                                  color: Colors.purple)),
                        ),
                        Container(
                          padding: const EdgeInsets.only(
                              left: 40, right: 40, top: 15),
                          child: TextFormField(
                            decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                hintText: 'Masukkan Pertanyaan',
                                hintStyle: TextStyle(color: Colors.deepPurple),
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 5, horizontal: 15)),
                            onSaved: (value) =>
                                setState(() => questionField = value!),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Pertanyaan tidak boleh kosong';
                              } else {
                                return null;
                              }
                            },
                            maxLength: 200,
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.only(top: 20),
                          alignment: Alignment.center,
                          child: const Text("Opsi 1",
                              style: TextStyle(
                                  fontFamily: 'Satisfy-Regular',
                                  // fontSize: 45,
                                  color: Colors.purple)),
                        ),
                        Container(
                          padding: const EdgeInsets.only(
                              left: 40, right: 40, top: 15),
                          child: TextFormField(
                            decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                hintText: 'Masukkan Opsi Pertama',
                                hintStyle: TextStyle(color: Colors.deepPurple),
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 5, horizontal: 15)),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Opsi Pertama tidak boleh kosong';
                              } else {
                                return null;
                              }
                            },
                            maxLength: 200,
                            onSaved: (value) =>
                                setState(() => op1Field = value!),
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.only(top: 20),
                          alignment: Alignment.center,
                          child: const Text("Opsi 2",
                              style: TextStyle(
                                  fontFamily: 'Satisfy-Regular',
                                  // fontSize: 45,
                                  color: Colors.purple)),
                        ),
                        Container(
                          padding: const EdgeInsets.only(
                              left: 40, right: 40, top: 15),
                          child: TextFormField(
                            decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                hintText: 'Masukkan Opsi Kedua',
                                hintStyle: TextStyle(color: Colors.deepPurple),
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 5, horizontal: 15)),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Opsi Kedua tidak boleh kosong';
                              } else {
                                return null;
                              }
                            },
                            maxLength: 200,
                            onSaved: (value) =>
                                setState(() => op2Field = value!),
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.only(top: 20),
                          alignment: Alignment.center,
                          child: const Text("Opsi 3",
                              style: TextStyle(
                                  fontFamily: 'Satisfy-Regular',
                                  // fontSize: 45,
                                  color: Colors.purple)),
                        ),
                        Container(
                          padding: const EdgeInsets.only(
                              left: 40, right: 40, top: 15),
                          child: TextFormField(
                            decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                hintText: 'Masukkan Opsi Ketiga',
                                hintStyle: TextStyle(color: Colors.deepPurple),
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 5, horizontal: 15)),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Opsi Ketiga tidak boleh kosong';
                              } else {
                                return null;
                              }
                            },
                            maxLength: 200,
                            onSaved: (value) =>
                                setState(() => op3Field = value!),
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.only(top: 20),
                          alignment: Alignment.center,
                          child: const Text("Opsi 4",
                              style: TextStyle(
                                  fontFamily: 'Satisfy-Regular',
                                  // fontSize: 45,
                                  color: Colors.purple)),
                        ),
                        Container(
                          padding: const EdgeInsets.only(
                              left: 40, right: 40, top: 15),
                          child: TextFormField(
                            decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                hintText: 'Masukkan Opsi Keempat',
                                hintStyle: TextStyle(color: Colors.deepPurple),
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 5, horizontal: 15)),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Opsi Keempat tidak boleh kosong';
                              } else {
                                return null;
                              }
                            },
                            maxLength: 200,
                            onSaved: (value) =>
                                setState(() => op4Field = value!),
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.only(top: 20),
                          alignment: Alignment.center,
                          child: const Text("Jawaban",
                              style: TextStyle(
                                  fontFamily: 'Satisfy-Regular',
                                  // fontSize: 45,
                                  color: Colors.purple)),
                        ),
                        Container(
                          padding: const EdgeInsets.only(
                              left: 40, right: 40, top: 15),
                          child: TextFormField(
                            decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                hintText: 'Masukkan Jawaban',
                                hintStyle: TextStyle(color: Colors.deepPurple),
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 5, horizontal: 15)),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Jawaban tidak boleh kosong';
                              } else {
                                return null;
                              }
                            },
                            maxLength: 200,
                            onSaved: (value) =>
                                setState(() => ansField = value!),
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.only(top: 20),
                          alignment: Alignment.center,
                          child: const Text("Pembahasan",
                              style: TextStyle(
                                  fontFamily: 'Satisfy-Regular',
                                  // fontSize: 45,
                                  color: Colors.purple)),
                        ),
                        Container(
                          padding: const EdgeInsets.only(
                              left: 40, right: 40, top: 15),
                          child: TextFormField(
                            decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                hintText: 'Masukkan Pembahasan',
                                hintStyle: TextStyle(color: Colors.deepPurple),
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 5, horizontal: 15)),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Pembahasan tidak boleh kosong';
                              } else {
                                return null;
                              }
                            },
                            maxLength: 200,
                            onSaved: (value) =>
                                setState(() => pembahasanField = value!),
                          ),
                        ),
                        Container(
                          height: 60,
                          padding: const EdgeInsets.only(
                              top: 20, left: 70, right: 70),
                          child: ElevatedButton(
                            child: const Text('Enter'),
                            onPressed: () async {
                              final isValid = formKey.currentState!.validate();
                              // FocusScope.of(context).unfocus();

                              if (isValid) {
                                formKey.currentState!.save();

                                Map data = {
                                  'question': questionField,
                                  'op1': op1Field,
                                  'op2': op2Field,
                                  'op3': op3Field,
                                  'op4': op4Field,
                                  'ans': ansField,
                                  'pembahasan': pembahasanField,
                                };
                                formKey.currentState!.reset();

                                String body = convert.jsonEncode(data);
                                var url = Uri.parse(
                                    'https://kuiskuy.herokuapp.com/create/questionSimpleFlutter');
                                http.Response response = await http.post(
                                  url,
                                  body: body,
                                );

                                final message = 'Pertanyaan berhasil ditambahkan';
                                final snackBar = SnackBar(
                                  content: Text(
                                    message,
                                    style: TextStyle(fontSize: 20),
                                  ),
                                  backgroundColor: Colors.green,
                                );
                                ScaffoldMessenger.of(context)
                                    .showSnackBar(snackBar);
                              }
                            },
                          ),
                        )
                      ],
                    )),
                  ))
            ],
          )),
    );
  }
}
