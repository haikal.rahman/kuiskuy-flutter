import 'package:flutter/material.dart';
import 'package:kuiskuy_flutter/bareng_bareng/drawer_kuiskuy.dart';
import 'package:kuiskuy_flutter/bareng_bareng/appbar_kuiskuy.dart';

class PembahasanScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const DrawerKuiskuy(),
      appBar: const AppbarKuiskuy("Kuis"),
      body: Container(
        height: 1000,
        width: double.infinity,
        child: Text(
            ' Berikut merupakan Jawaban dari kuis sebelumnya \n 1.Flutter is an open-source UI software development kit created by Google \n 2. May 2017 Google Release Flutter \n 3. A memory location that holds a single letter or number is Char \n 4. A command that we use to output data to the screen is Cout'),
        decoration: const BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [Colors.lightBlue, Colors.purple]),
        ),
      ), //Container
    ); //Scaffold
  }
}
