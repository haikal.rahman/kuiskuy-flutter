import 'package:flutter/material.dart';
import 'package:kuiskuy_flutter/bareng_bareng/user_data.dart';
import 'package:kuiskuy_flutter/bareng_bareng/drawer_kuiskuy.dart';
import 'package:kuiskuy_flutter/bareng_bareng/appbar_kuiskuy.dart';
import 'package:kuiskuy_flutter/page_profil/edit_profile_page.dart';
import 'package:kuiskuy_flutter/page_profil/review_kuis.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'dart:math';

class ProfileSiswa extends StatelessWidget {
  const ProfileSiswa({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MyHomePage(title: "Profile Siswa");
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  bool _scrollState = false;
  String nama = UserData().nama;
  String noInduk = UserData().noInduk;
  String email = UserData().email;
  String status = UserData().status;
  Map<String, Map<String, Map<String, String>>> dataFetch =
      UserData().matakuliah;
  List<List<dynamic>> data = [];

  late ScrollController _scrollController;
  late TrackballBehavior _trackballBehavior;

  @override
  void initState() {
    _scrollController = ScrollController()
      ..addListener(() {
        if (_scrollController.offset >= MediaQuery.of(context).size.height) {
          _scrollState = true; //yang muncul tombol to back-to-top
        } else {
          _scrollState = false; //yang muncul tombol to back-to-bottom
        }
      });
    _trackballBehavior = TrackballBehavior(
        enable: true,
        tooltipDisplayMode: TrackballDisplayMode.floatAllPoints,
        tooltipSettings:
            const InteractiveTooltip(enable: true, format: 'point.x\npoint.y'));
    super.initState();
  }

  //[[matkul1, [[kuis1, nilai, tgl], [kuis2, nilai, tgl]]],...]
  List<List<dynamic>> _listify() {
    List<List<dynamic>> hasil = [];
    Iterable<String> matkulKeys = dataFetch.keys;
    for (int i = 0; i < matkulKeys.length; i++) {
      List<dynamic> mat = [];
      mat.add(matkulKeys.elementAt(i));
      List<List<String>> ku = [];
      Iterable<String> kuisKeys = dataFetch[matkulKeys.elementAt(i)]!.keys;
      for (int j = 0; j < kuisKeys.length; j++) {
        List<String> detail = [];
        detail.add(kuisKeys.elementAt(j));
        detail.add(dataFetch[matkulKeys.elementAt(i)]![kuisKeys.elementAt(j)]![
            "nilai"]!);
        detail.add(dataFetch[matkulKeys.elementAt(i)]![kuisKeys.elementAt(j)]![
            "tgl"]!);
        ku.add(detail);
      }
      mat.add(ku);
      hasil.add(mat);
    }
    return hasil;
  }

  List<StackedLineSeries<List<String>, DateTime>> _series() {
    data = _listify();
    List<StackedLineSeries<List<String>, DateTime>> ret = [];
    for (int i = 0; i < data.length; i++) {
      ret.add(StackedLineSeries(
        name: data[i][0],
        animationDuration: 0,
        groupName: data[i][0],
        legendIconType: LegendIconType.diamond,
        markerSettings: const MarkerSettings(isVisible: true),
        dataLabelSettings: const DataLabelSettings(
          useSeriesColor: true,
        ),
        dataSource: data[i][1],
        xValueMapper: (List<String> input, _) => DateTime.parse(input[2]),
        yValueMapper: (List<String> input, _) => double.parse(input[1]),
      ));
    }
    return ret;
  }

  List<DataColumn> _column() {
    List<DataColumn> ret = [];
    int curr = 0;
    for (int i = 0; i < data.length; i++) {
      curr = max(curr, data[i][1].length);
    }
    ret.add(const DataColumn(label: Text("Mata Pelajaran")));
    for (int i = 1; i < curr + 1; i++) {
      ret.add(DataColumn(label: Text(i.toString())));
    }
    return ret;
  }

  List<DataRow> _row() {
    List<DataRow> ret = [];
    List<String> months = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    ];
    int curr = 0;
    for (int i = 0; i < data.length; i++) {
      curr = max(curr, data[i][1].length);
    }
    for (int i = 0; i < data.length; i++) {
      List<DataCell> cell = [];
      cell.add(DataCell(
          SizedBox(
            child: Text(
              data[i][0],
              softWrap: true,
            ),
            width: 120
          )
        )
      );
      for (int j = 0; j < curr; j++) {
        if (j < data[i][1].length){
          String namaKuis = data[i][1][j][0];
          String tgl = data[i][1][j][2];
          tgl = months[int.parse(tgl.substring(5, 7)) - 1] +
              " " +
              tgl.substring(8) +
              ", " +
              tgl.substring(0, 4);
          cell.add(DataCell(
              SizedBox(
                child: Text(
                  namaKuis + "\n" + tgl,
                  softWrap: true,
                ),
                width: 140
              )
            )
          );
        } else{
          cell.add(
            const DataCell(
              SizedBox(
                child: Text("-"),
                width: 140
              )
            )
          );
        }
      }
      ret.add(DataRow(cells: cell));
    }
    return ret;
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _scrollToBottom() {
    _scrollController.animateTo(_scrollController.position.maxScrollExtent,
        duration: const Duration(seconds: 1), curve: Curves.easeOut);
    setState(() {
      _scrollState = true;
    });
  }

  void _scrollToTop() {
    _scrollController.animateTo(0,
        duration: const Duration(seconds: 1), curve: Curves.easeOut);
    setState(() {
      _scrollState = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const DrawerKuiskuy(),
      appBar: const AppbarKuiskuy("Your Profile"),
      body: SingleChildScrollView(
        physics: const NeverScrollableScrollPhysics(),
        controller: _scrollController,
        child: Container(
          child: Column(
            children: <Widget>[
              Container(
                  child: Column(
                    children: [
                      Image.asset(
                        'assets/images/gray_transparent_default_profile.png',
                        height: 130,
                        width: 130,
                      ),
                      const SizedBox(height: 15),
                      Text(nama,
                          style: const TextStyle(
                            fontSize: 25,
                            color: Colors.black,
                          )),
                      const SizedBox(height: 5),
                      const Text('1234567891',
                          style: TextStyle(color: Colors.purple, fontSize: 15)),
                      const SizedBox(height: 5),
                      Text(status),
                      const SizedBox(height: 5),
                      const Text("uzumakiB4mbang@hotmail.com",
                          style: TextStyle(
                              color: Colors.blueAccent, fontSize: 15)),
                      const SizedBox(height: 15),
                      Align(
                        alignment: Alignment.center,
                        child: Tooltip(
                          message: "Edit Profile Anda",
                          waitDuration: const Duration(milliseconds: 500),
                          preferBelow: false,
                          child: TextButton(
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            const EditProfilePage()));
                              },
                              child: Stack(
                                children: [
                                  Container(
                                      child: const Icon(Icons.edit_outlined,
                                          color: Colors.white),
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Theme.of(context)
                                              .colorScheme
                                              .secondary),
                                      padding: const EdgeInsets.all(10))
                                ],
                              )),
                        ),
                      ),
                      const SizedBox(height: 15),
                      const Text('Recent Quiz'),
                      const SizedBox(height: 5),
                      Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: const [
                              Text("BST", softWrap: true),
                              Text("80/100", softWrap: true),
                              Text("SDA", softWrap: true),
                            ],
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(18),
                              gradient: const LinearGradient(
                                  begin: Alignment.centerLeft,
                                  end: Alignment.centerRight,
                                  colors: [Colors.lightBlue, Colors.purple])),
                          padding: const EdgeInsets.symmetric(
                              vertical: 5, horizontal: 5)),
                      const SizedBox(height: 10),
                      Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: const [
                              Text("RVU", softWrap: true),
                              Text("70/100", softWrap: true),
                              Text("Alin", softWrap: true),
                            ],
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(18),
                              gradient: const LinearGradient(
                                  begin: Alignment.centerLeft,
                                  end: Alignment.centerRight,
                                  colors: [Colors.lightBlue, Colors.purple])),
                          padding: const EdgeInsets.symmetric(
                              vertical: 5, horizontal: 5)),
                      const SizedBox(height: 10),
                      Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: const [
                              Text("Synchronization", softWrap: true),
                              Text("40/100", softWrap: true),
                              Text("OS", softWrap: true),
                            ],
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(18),
                              gradient: const LinearGradient(
                                  begin: Alignment.centerLeft,
                                  end: Alignment.centerRight,
                                  colors: [Colors.lightBlue, Colors.purple])),
                          padding: const EdgeInsets.symmetric(
                              vertical: 5, horizontal: 5)),
                    ],
                  ),
                  padding: const EdgeInsets.fromLTRB(10, 30, 10, 3),
                  decoration: BoxDecoration(
                      color: const Color.fromRGBO(211, 215, 248, 0.6),
                      borderRadius: BorderRadius.circular(18),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.06),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: const Offset(0, 3),
                        )
                      ]),
                  width: double.infinity,
                  height: 530),
              const SizedBox(height: 250),
              Container(
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 50,
                        child: Text("Hasil Kuis",
                            style: TextStyle(
                              fontSize: 25,
                              color: Colors.black,
                            )),
                      ),
                      SingleChildScrollView(
                          child: SizedBox(
                              child: Column(children: [
                                SfCartesianChart(
                                    trackballBehavior: _trackballBehavior,
                                    primaryXAxis: DateTimeAxis(
                                      intervalType: DateTimeIntervalType.days,
                                      interval: 15,
                                    ),
                                    series: _series(),
                                    borderColor: Colors.white,
                                    plotAreaBorderColor: Colors.grey,
                                    legend: Legend(
                                        isVisible: true,
                                        position: LegendPosition.bottom,
                                        overflowMode:
                                            LegendItemOverflowMode.scroll)),
                              ]),
                              width: 800),
                          scrollDirection: Axis.horizontal),
                      const SizedBox(height: 15),
                      Center(
                        child: SizedBox(
                          child: SingleChildScrollView(
                            child: SingleChildScrollView(
                              child: DataTable(
                                columns: _column(),
                                rows: _row(),
                                dataRowHeight: 60,
                                columnSpacing: 15,
                                decoration: BoxDecoration(
                                  color: const Color.fromARGB(70, 255, 255, 255),
                                  border: Border.all(color: Colors.white)
                                )
                              ),
                              scrollDirection: Axis.horizontal
                            ),
                          scrollDirection: Axis.vertical,
                          ),
                          height: 90,
                        ) 
                      )
                    ],
                  ),
                  padding: const EdgeInsets.fromLTRB(10, 30, 10, 3),
                  decoration: BoxDecoration(
                      color: const Color.fromRGBO(211, 215, 248, 0.6),
                      borderRadius: BorderRadius.circular(18),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.06),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: const Offset(0, 3),
                        )
                      ]),
                  width: double.infinity,
                  height: 530),
              const SizedBox(height: 70),
            ],
          ),
          decoration: const BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.lightBlue, Colors.purple]),
          ),
          alignment: Alignment.center,
          padding: const EdgeInsets.symmetric(vertical: 35, horizontal: 20),
        ),
      ),
      floatingActionButton: _scrollState == false
          ? FloatingActionButton(
              heroTag: 'ScrollBtn',
              onPressed: _scrollToBottom,
              child: const Icon(Icons.arrow_downward_rounded),
              tooltip: "Lihat nilai keseluruhan anda")
          : Row(
            children: [
              FloatingActionButton(
                heroTag: 'ScrollBtn',
                onPressed: _scrollToTop,
                child: const Icon(Icons.arrow_upward_rounded),
                backgroundColor: Colors.blue,
                tooltip: "Kembali ke profil",
              ),
              const SizedBox(width: 15),
              FloatingActionButton.extended(
                heroTag: 'ScrollBtn',
                backgroundColor: Colors.blue,
                onPressed: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>
                        const ReviewPage()
                      )
                  );
                },
                label: const Text('Review'),
                tooltip: "Review kuis-kuis Anda",
              ),
            ],
            mainAxisAlignment: MainAxisAlignment.center,
          ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
